from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User, Tweet


class RegisterUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'full_name')


class UpdateUserForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'full_name')


class PostTweetForm(forms.ModelForm):
    class Meta:
        model = Tweet
        exclude = ('user',)
        widgets = {
            'body': forms.Textarea(attrs={
                'cols': 60,
                'rows': 4,
                'placeholder': "What's happening?",
            })
        }
        labels = {
            'body': '',
        }
