from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.db.models import Q, Count

from .models import User, Tweet, Profile
from .forms import RegisterUserForm, PostTweetForm


# Registration
def register(request):
    # handle registration
    if request.method == 'POST':
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            # Create profile for user
            profile = Profile(user=user)
            profile.save()
            return redirect('login')
    else:
        form = RegisterUserForm()
    # render view
    context = {
        'form': form,
    }
    return render(request, 'registration/register.html', context)


# Main views
def index(request):
    if not request.user.is_authenticated:
        # render 'welcome' view for guests
        return render(request, 'twitter/welcome.html')
    # Get tweets from followed users and currently logged in user
    tweets = Tweet.objects.filter(
            Q(user__in=request.user.following.values('user'))
            | Q(user=request.user)
        ).order_by('-date')

    context = {
        'post_tweet_form': PostTweetForm(),
        'tweets': tweets,
    }
    return render(request, 'twitter/index.html', context)


def profile(request, username):
    user = get_object_or_404(User, username=username)
    profile = get_object_or_404(Profile, user=user)
    tweets = profile.user.tweets.order_by('-date')
    context = {
        'profile': profile,
        'tweets': tweets,
    }

    if request.user.is_authenticated:
        context['is_following'] = bool(request.user.following.filter(user=user))
    return render(request, 'twitter/profile.html', context)


def people(request):
    # default list
    context = {
        'people': Profile.objects.all()
    }

    if request.GET.get('followed_by'):
        # people followed by <user>
        user = get_object_or_404(User, username=request.GET['followed_by'])
        context['custom_title'] = f'People followed by {user.full_name}'
        context['people'] = user.following.all()
    elif request.GET.get('following'):
        # people following <user>
        user = get_object_or_404(User, username=request.GET['following'])
        context['custom_title'] = f'People following {user.full_name}'
        context['people'] = Profile.objects.filter(
                                user__in=user.profile.followers.all())

    # order all people by their follower count
    context['people'] = context['people']\
        .annotate(num_followers=Count('followers'))\
        .order_by('-num_followers', 'user__full_name')
    return render(request, 'twitter/people.html', context)


@login_required
def settings(request):
    msg = ''
    # update account
    if request.method == 'POST':
        full_name = request.POST.get('full_name', '')
        about = request.POST.get('about', '')
        if not 0 < len(full_name) <= 100:
            msg = 'Error: Full name is required can be at most 100 characters long.'
        else:
            user = request.user
            user.full_name = full_name
            user.profile.about = about
            user.save()
            user.profile.save()
            msg = 'Changes saved.'

    # render view
    context = {
        'form_username': request.user.username,
        'form_full_name': request.user.full_name or '',
        'form_about': request.user.profile.about or '',
        'form_message': msg,
    }
    return render(request, 'twitter/settings.html', context)


# Actions
@login_required
def post_tweet(request):
    if request.method == 'POST':
        tweet_form = PostTweetForm(request.POST)
        if tweet_form.is_valid():
            tweet_body = tweet_form.cleaned_data['body']
            tweet = Tweet(user=request.user, body=tweet_body)
            tweet.save()
            return redirect('index')


@login_required
def delete_tweet(request, tweet_id):
    tweet = Tweet.objects.get(pk=tweet_id)
    tweet.delete()
    # return to previous page
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def follow_user(request, username):
    user = get_object_or_404(User, username=username)
    # follow or unfollow user
    if request.user.following.filter(user=user):
        request.user.following.remove(user.profile)
    else:
        request.user.following.add(user.profile)
    # return to previous page
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
