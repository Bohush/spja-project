from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

import textwrap


class User(AbstractUser):
    username = models.CharField(max_length=30, unique=True)
    full_name = models.CharField(max_length=100)

    def __str__(self):
        return self.username

    def tag(self):
        return f'@{self.username}'

    def short_date_joined(self):
        return self.date_joined.strftime('%B %Y')

    def following_count(self):
        return self.following.count()

    def tweet_count(self):
        return self.tweets.count()


class Tweet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='tweets')
    body = models.TextField()
    date = models.DateTimeField('date tweeted', auto_now_add=True)

    def __str__(self):
        body = textwrap.shorten(self.body, width=30, placeholder='...')
        return f'{self.user.tag()}: {body}'

    def short_date(self):
        now = timezone.now()
        delta = now - self.date
        if self.date.year < now.year:
            # Tweet from previous year
            # Example: Jan 1 2017
            return '{0:%b} {0.day} {0.year}'.format(self.date)
        elif delta.total_seconds() < 3600:
            # Posted less than an hour ago
            # Example: 20m
            return f'{round(delta.seconds / 60)}m'
        elif delta.days <= 0:
            # Posted less than 24 hours ago
            # Example: 15h
            return f'{round(delta.seconds / 3600)}h'
        else:
            # Tweet from this year
            # Example: Jan 1
            return '{0:%b} {0.day}'.format(self.date)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    about = models.TextField(null=True, blank=True)
    followers = models.ManyToManyField(User, related_name='following')

    def __str__(self):
        return f"{self.user}'s profile"

    def follower_count(self):
        return self.followers.count()
