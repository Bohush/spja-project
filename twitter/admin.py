from django.contrib import admin

from .models import User, Tweet, Profile
from .forms import RegisterUserForm, UpdateUserForm


class UserAdmin(admin.ModelAdmin):
    """User in administration page (list of users, add user, change user)."""
    # Basic Info
    model = User
    list_display = ('username', 'full_name', 'is_staff', 'is_active')
    search_fields = ('username', 'full_name')

    # Add User Form
    add_form = RegisterUserForm
    add_fieldsets = (
        (None, {
            'fields': ('username', 'full_name', 'password1', 'password2'),
        }),
    )

    # Change User Form
    form = UpdateUserForm
    fieldsets = (
        (None, {
            'fields': ('username', 'full_name', 'password')
        }),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser',
                       'user_permissions')
        }),
        ('Important dates', {
            'fields': ('last_login', 'date_joined')
        })
    )


class TweetAdmin(admin.ModelAdmin):
    """Tweet in administration page."""
    model = Tweet
    fieldsets = (
        (None, {
            'fields': ('user', 'body')
        }),
    )


class ProfileAdmin(admin.ModelAdmin):
    """Profile in administration page."""
    model = Profile
    list_display = ('__str__', 'follower_count')
    fieldsets = (
        (None, {
            'fields': ('user', 'about')
        }),
    )


admin.site.register(User, UserAdmin)
admin.site.register(Tweet, TweetAdmin)
admin.site.register(Profile, ProfileAdmin)
