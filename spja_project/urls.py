"""spja_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import twitter.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', twitter.views.index, name='index'),
    path('register/', twitter.views.register, name='register'),
    path('settings/', twitter.views.settings, name='settings'),
    path('people/', twitter.views.people, name='people'),
    path('post_tweet/', twitter.views.post_tweet, name='post_tweet'),
    path('delete_tweet/<int:tweet_id>', twitter.views.delete_tweet, name='delete_tweet'),
    path('<str:username>', twitter.views.profile, name='profile'),
    path('follow_user/<str:username>', twitter.views.follow_user, name='follow_user'),
]
